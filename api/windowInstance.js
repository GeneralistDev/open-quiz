let win = null;

const setWindowInstance = function (win) {
  win = win;
}

const getWindowInstance = function() {
  return win;
}

module.exports = {
  setWindowInstance: setWindowInstance,
  getWindowInstance: getWindowInstance,
};