'use strict';
const uuid = require('uuid/v4');

module.exports = (sequelize, DataTypes) => {
  const Quiz = sequelize.define('Quiz', {
    id: { type: DataTypes.STRING, allowNull: false, primaryKey: true, defaultValue: uuid() },
    name: DataTypes.STRING,
  }, {});
  Quiz.associate = function(models) {
    Quiz.hasMany(models.QuizQuestion, { as: 'Questions' });
  };
  return Quiz;
};